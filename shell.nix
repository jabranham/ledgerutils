let
  pkgs = import <nixpkgs> {};
in with pkgs; {
  simpleEnv = stdenv.mkDerivation {
    name = "ledgerutils-env";
    version = "1";
    buildInputs = [
      ledger
      R
      rPackages.magrittr
      rPackages.quantmod
      rPackages.dplyr
      rPackages.lubridate
      rPackages.ggplot2
      rPackages.testthat
      rPackages.tidyr
      which
     ];
  };
}
